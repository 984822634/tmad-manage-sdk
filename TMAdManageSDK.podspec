#
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
# .framework/Headers/*.{h}


Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "TMAdManageSDK"
  s.version      = "0.1.94"
  s.summary      = "广告弹框"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/984822634/tmad-manage-sdk.git", :tag => s.version }
  s.source_files = 'TMAdManageSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMAdManageSDK.framework'
  s.resources = "TMadManage.bundle"
  s.requires_arc = true


  s.dependency "Masonry"
  s.dependency "TMSDK"
  s.dependency "SDWebImage"
  s.dependency "TMUserCenter"

s.xcconfig = {
  'VALID_ARCHS' =>  'armv7 x86_64 arm64',
}


end
